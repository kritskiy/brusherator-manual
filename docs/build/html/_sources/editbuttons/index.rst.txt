Edit Buttons and their arrangement
==================================

This section is about editing individual button properties and customizing your panel.

To edit button settings, chose ``Edit button...`` from RMB-menu or Alt+Click the button:

.. image:: img/start.png

-----

.. _editbuttons:

Edit Button window structure resembles the :ref:`Add Button window <addwindow>`.

* The assigned preset and its type are indicated in the first section
* The second section is used to change the appearance of the button
* The third section is used to update preset setting or change new appearance options

.. image:: img/editbutton.png

-------

Button Appearance
-----------------

* Select a different appearance type to recreate the button with different name, background color or update the thumbnail. |br|\
* To change a name or a button title for thumbnail button, select ``Custom image``, use ``Current Thumb`` and change the name in ``Name on the panel`` field:

.. image:: img/changename.png

-------

Update thumbnails
~~~~~~~~~~~~~~~~~

``Edit thumbnail`` will open open thumbnail of the button for editing in PS, ``Edit stroke preview`` will open stroke preview for Brusherator Preview.

.. image:: img/stroke.png

-------

.. _updatePreset:

Update Preset and Create/Update Alternative
-------------------------------------------

Current Brush ot Tool Preset can be updated directly from Edit Button window of Brusherator. Use ``Update the preset`` button to replace the preset, assigned to the button, with active instrument. :ref:`Alternatives <alternatives>` also can be created or updated from this window.

---------

.. _editmode:

Edit Mode
---------

In Edit mode you can

* resize any button
* grab and move buttons around
* align them
* match size/color
* rearrange shelves (if you have ``Show Shelves as Tabs`` option enabled)

Hit Alt+Click on empty space, use RMB-menu or Edit Mode button in the Bottom Menu to switch to Edit Mode.

.. image:: img/editmode.png

---------

Selecting and moving buttons
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Draw rectangular selection, add or substract from selection with Ctrl/Cmd+Click. |br|\
Move buttons freely.

.. image:: img/marque.gif

Note that in older *CC2013* ``Ctrl``/``Cmd+Click`` won't work, there's a ``Add to Selection`` item in button RMB-menu.

---------

Resizing
~~~~~~~~

Grab and move a handler in the bottom-right of a button to resize it.

---------

Aligning, stacking
~~~~~~~~~~~~~~~~~~~~~

Use ``Align`` submenu to align or stack buttons together

.. image:: img/repack.gif

---------

Matching Size and Color
~~~~~~~~~~~~~~~~~~~~~~~

Use ``Match`` submenu to match buttons size or background color. The button on which the submenu was called will be a reference for size or color.

.. image:: img/match.gif

---------

Transfering buttons between shelves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Select the buttons you want to copy or move to a different shelf and use ``Transfer Buttons`` command from Fly-out menu (or Bottom menu for CC2013)

.. image:: img/transfer.png

------------

Deleting Buttons
----------------

Shift+Click on a button or use ``Delete button`` RMB-menu command to delete a button from the panel. |br|\
While in Edit Mode you can delete several selected buttons with RBM-menu command.

.. image:: img/delete.png

* ``Only the button`` will delete the button from Brusherator without deleting the preset and thumbnails (useful if you have the same button on several shelves)
* ``Button and thumbnails`` will delete the button and all the thumbnails, but leave the preset in Photoshop
* ``Button, thumbnail and preset`` will also delete the Preset from Photoshop

.. |br| raw:: html

   <br />