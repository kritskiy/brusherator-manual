.. _historator:

Brusherator History
========================

Brusherator History is a panel that shows recently used tool and brush presets. Tools may be marked as favourites so they could stay be in the list or added to Brusherator, hotkeys may be assigned to specific slots (up to 10). Tools can have override settings option. This panel will only work in Photoshops CC2015 and newer.

.. image:: img/hist1.gif

* Are you using a specific several brushes for a project? Mark them as favourite so they'd stay visible;
* Need to reorganize the list? Simply drag and drop the items to sort the list;
* Do you have a favourite brush and want to assign it to a hotkey? Mark it as favourite and assign a hotkey to that slot;
* Do you want to temporary change a specific setting of a preset? Use the Tool Override option;
* Are you going through a list of downloaded brushes and trying to remember which one you liked? Simply mark them as favourite and move to Brusherator later using a :ref:`Add from History favourites <fromhistory>` option;

--------------------------------

Options
--------------------------------

.. image:: img/panel-h.png

Assign Hotkeys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here you can assign hotkeys for one of the ten specific Brusherator History slots.

.. image:: img/hist2.gif

--------------------------------

Change Number of Buttons Displayed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This will allow you to show a specific number of buttons

--------------------------------

Show Tool Icon
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toggles tool icons visible on and off

--------------------------------

Show Tool Override Icon
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toggles Tool Override icon on and off. To use the Tool Override:

#. Select a preset
#. Change a setting you want to override
#. Click the override icon

Now clicking on the preset item in the History panel will load the overriden tool. To remove the override, ``Shift+Click`` or ``RMB-click`` the override icon;

.. More about the override in this video:

--------------------------------

Use Brusherator Color as Backgrounds
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This will toggle a background color between grey and the one used on Brusherator button

--------------------------------

Show Hotkey Icons
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This will toggle hotkey icons on and off

--------------------------------

Preserve Brushes Size
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This will toggle preserving a size between different brushes no matter of a size in the preset

--------------------------------

.. |br| raw:: html

   <br />