Release Log
===========

.. * New Shapes to Shelf function;

27 Jan 2024: Brusherator 1.8.2
--------------------------------

* Fixed: Photoshop 24.5 breaking everything

+ New options in the Add Menu Item list
+ Both Add and Edit functions for Custom Thumbnails allow setting the button background color;

--------------------------------


05 Dec 2022: Brusherator 1.8.1
-------------------------------------

+ Brushes Duplicates window has a ``Don't show again`` option;
+ Some Fixes;

--------------------------------


25 November 2021: Brusherator 1.8.0
------------------------------------

Note that the manual hasn't been updated with the new features of 1.8, will do that later;

+ Tags and Tags Manager for buttons;
+ Option to ``Select from Groups`` when adding Brush Presets;
+ New setting: Show Buttons Tooltips;
+ Brusherator History: new Tool Override setting;
+ new embossed buttons look;

* Changed: Actions are no longer exported with a shelf: this was causing freezes for some cases. Actions should be exported manually now.

+ Some missing tools and menu items were added to ``Add from Menu items`` section (Curvature Pen, Select Object);
+ Bugfixes;

--------------------------------

14 Aug 2019: Brusherator 1.7.1
--------------------------------

* Fixed: Brusherator History scripts were unassignable via the panel interface

--------------------------------

14 Aug 2019: Brusherator 1.7.0
--------------------------------

+ It's possible to change Data Folder location to a different folder (i.e. for syncing) using the ``Open Data Folder Options...`` command from the Flyout Menu
+ New ``Mute Panel Colors`` setting and script to toggle panel buttons background color on and off
+ You can sort Brusherator History list by dragging the items 
+ Number of assignable slots on Brusherator History was increased to 10
+ Scripts that aren't part of `Toggletator <https://toggletator-manual.readthedocs.io/en/latest/>`_ and `Scriptorator <https://scriptorator.readthedocs.io/en/latest/index.html>`_ have a ``Reveal Location`` button in the Edit Button window

* Fixed: Using the `Select previous/next brush` functions (`,`/`.` keys by default) was resulting in empty items in Brusherator History

--------------------------------

19 June 2019: Brusherator 1.6.0
--------------------------------

+ Export all shelves in one go from Manage Shelves window
+ New ``Preserve Brush Settings`` setting: preserve opacity, flow, blending mode, wetness, strength when selecting brush or tool presets with Shift key (CC2015.5+)
+ Assing hotkeys to Select Alternative, Cycle Tools, Toggle BT/BTP/BTP2 Panels directly from Brusherator panel (from the flyout menu)

* Fixed: When an action UI window is cancelled, error message appearing
* Fixed: Too many folders inside the Scripts folder crashing PS
* Fixed: Too many buttons in the same time ctashing Brusherator

--------------------------------

21 May 2019: Brusherator 1.5.3
--------------------------------

* Fixed: adding Actions and Scripts was broken in some cases

--------------------------------

17 May 2019: Brusherator 1.5.2
--------------------------------

+ When adding new Brush Preset from the active tool it's possible to Capture Tool so it'd act as a tool preset (CC2018 and newer)

* Fixed: included scripts not working
* Fixed: UI clashing in Add New Button window

--------------------------------

2 May 2019: Brusherator 1.5.1
--------------------------------

+ New option when adding buttons: Create Preview (off by default), allows to create buttons faster. You can create the previews later from Edit Button window or from RMB menu in Edit Mode
+ It's possible to set names for several selected buttons in Add Button dialog

* Fixed: weird horizontal line on top when only one shelf is available and "show shelves as tabs" turned on
* Fixed: certain scripts not working when placed on Brusherator
* Fixed: minimum size for Brusherator History has been reduced to 50px

--------------------------------

4 Mar 2019: Brusherator 1.5.0
--------------------------------

+ Add brushes and tools by clicking on them: ``Add from History`` was transformed to ``Add from History or selected``;
+ :ref:`Add Menu Items <addmenuitemfly>` command now includes commands from the flyout menus;
+ It's possible to match width and height of a reference button in one action;
+ Better integration with `Toggletator <https://toggletator-manual.readthedocs.io/en/latest/>`_ and `Scriptorator <https://scriptorator.readthedocs.io/en/latest/index.html>`_: ``Ctrl/Cmd+Click`` on a script button doesn't try to look for an alternative for a script, it's possible to open Scripts Settings directly from Edit Button window and the flyout menus;
+ New ``Item Title`` field when creating and editing buttons for custom buttons titles;

* fixed Export and Import shelf issues;
* fixed bottom settings menu not working;
* fixed ``Save`` menu command working as ``Save As``;
* fixed ``Layer > Background`` menu command not working properly; 

--------------------------------

4 Sep 2018: Brusherator 1.4.1
--------------------------------

+ new :ref:`Add from History favourites <fromhistory>` option for adding buttons
+ Labels can be not only text, but also thumbnails
+ third Brusherador panel: Brusherator Plus 2

* Performance improvements
* Selected tool is visible on Brusherator History
* option to remove Alternative in the Edit Button window

+ Wrong placement of thumbnail button names fixed
+ Various small fixes 

--------------------------------

9 Aug 2018: Brusherator 1.4.0
--------------------------------

+ :ref:`New panel: Brusherator History (CC2015+) <historator>`
+ New item type: :ref:`text label <addtextlabel>`  
+ New item type: :ref:`menu item <addmenuitem>`
+ Text buttons can be resized
+ Shelves can be rearranged in :ref:`Edit Mode <editmode>`.

* Performance improvements
* Better buttons rendering
* Sample shelf updated with 1.4 features

+ Various fixes

--------------------------------

25 Oct 2017: Brusherator 1.3.5
------------------------------

+ Fixed Delete and Export for Brush Presets in CC2018
+ Fixed inability to change Previewrator panel size in PS CC2018
+ Fixed toggle_alternative script not working with Brush Presets
+ Fixed ``\`` symbol in shelf name causing troubles

--------------------------------

18 Oct 2017: Brusherator 1.3.4
------------------------------

+ Fixed inability to change panel size in PS CC2018
+ Fixed rare freezes on Win10
+ Scripts from symbolic links inside PS Scripts folder are imported correctly
+ I fixed something in bt_cycle script but I don't remember what
+ Installer installs additional scripts to CC2018 folder

--------------------------------

25 May 2017: Brusherator 1.3.3
------------------------------

+ Fixed an error with localised PS versions

--------------------------------

24 May 2017: Brusherator 1.3.2
------------------------------

* New documentation
* Panel settings are stored in Photoshop rather in Web Storage (this will reset current settings on first launch)
* Resize handler active area is smaller now, easier to move small buttons in Edit Mode
* bt_cycleTool will respect Preserve Brush Size option
* BT\_ and BTP\_ shelf switchers will reveal the panel if it's hidden and won't reload the panel if they're supposed to load the active shelf (please recreate your switchers with the updated BT_brushes.jsx)

+ No more scrollbars when tabs are visible
+ Resize to Content option respects tabs (in most cases lol)
+ Fixed Transfer Buttons for PS CC 2013
+ Fixed bt_cycleTools script for PS CC 2013
+ Buttons are created in correct coordinates in Tabbed mode
+ Current Thumb appearance option from Edit Button won't require an opened document to work

--------------------------------

17 Apr 2017: Brusherator 1.3.1
------------------------------

* ️Tooltips for all functions
* ️Tooltips on buttons will show the name of the preset they're linked to
* ️Scripts in Alias folders will appear in Select Scripts windows
* ️Scrollbars are in the color of Photoshop UI and smaller

+ Fixed some crashings on Windows 10
+ Sample panel only loads from Brusherator, not Brusherator Plus
+ No more persistent scrollbar when Show Shelves is enabled

--------------------------------

28 Mar 2017: Brusherator 1.3.0
------------------------------------------------

* Sample shelf for first-time users
* Scripts may be added on the panel as buttons
* Export/Import Shelves
* Delete several (selected) buttons in one go
* Add a button from last used preset
* Cycle between two last used buttons (with <b>bt_cycleTools</b>)
* Holding SHIFT while hitting "add from active tool" will create a new button using the last used settings
* Bottom-right menu disappears if the panel size is too small
* Set background color for auto-thumbnails
* Create/update alternative presets directly from Edit Button window
* Marquee select several buttons
* Align/stack buttons, match their size and background color
* Option to auto-resize Brusherator to panel content (CC2015 and above)

+ Bottom-right menu remembers its state
+ Alternative tools can use both prefix "a\_" and suffix "_a"
+ Buttons with missing tools marked with red overlay 
+ Buttons with missing thumbnails marked with red cross
+ GUI support all 4 colors of the Photoshop UI color
+ Better icons for light verision, icons are SVG now
+ All Brusherator scripts are now in separate category

* BT is more prone to weird shelf names
* Warnings for presets with emoji symbols (they'll cause errors) 
* No more weird behaviour of buttons with negative coordinates
* No more buttons stacking upon each other (buttons made in the same coordinates will spread out after panel reload)
* Buttons always respect the grid
* Fixed weird text rendering on Windows for text buttons
* Errors in shelf files won't cause the panel to break
* ExternalObject error

--------------------------------


05 Dec 2016: Brusherator 1.2.1
------------------------------

* Panel persistence is turned on

--------------------------------

05 Dec 2016: Brusherator 1.2.0
------------------------------

* Brusherator Plus: a second panel for more tools!
* Move several buttons at once
* Copy or move brushes between shelves
* Option to show shelves as tabs
* Create new tool and brush presets directly from Brusherator
* Rename and duplicate shelves
* Update current preset from Flyout menu
* More scripts for power users

+ ️New buttons are added under the cursor (when added from RMB menu/hotkey)

* ️Flyout menu of other extensions won't create shelves on Brusherator (whaaat)
* ️No more empty lines in shelves
* ️No need to switch off Current Tool Only Photoshop option anymore
* ️Wrong names of context menu items

--------------------------------

16 Nov 2016: Brusherator 1.1.0
------------------------------

* Actions may be placed on a panel as buttons
* Flyout menu (CC2014+) — striped small thing on the right of the panel
* You can select shelves from flyout menu
* Option to toggle brush size inheritance for all buttons (default: size is inherited)
* Option to toggle names visible on image buttons (default: off)
* Option to close a panel after button click (default: off)
* Name filter field in "Select Preset(s)" dialog is available on CC2015+ on OSX (may be slow!)
* You can open Brusherator data folder from flyout menu (for backups)

+ ️Brush selection is faster on Windows
+ ️"Name on the panel" field is available for all types of buttons (so you could type a name for image button)
+ ️"Use preset size" checkbox if you want to override brush size inheritance for specific tool
+ ️"Select Preset(s)" dialog is taller
+ ️Checker for Current shelf

* ️Button size ignores Photoshop UI font size option
* ️Moving/resizing buttons in Edit Mode won't trigger them
* ️Tablet Pen doesn't lose sensitivity in Windows

--------------------------------


11 Nov 2016: Brusherator 1.0.1
------------------------------

* Wrong thumbnails size when Photoshop rulers aren’t pixels
* Text buttons with spaces in their names jumping all over the place

--------------------------------


10 Nov 2016: Brusherator 1.0.0
------------------------------------