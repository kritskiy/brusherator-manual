.. _plus:

Brusherator Plus
==========================

Brusherator as a system consists of 5 panels:

* Brusherator
* Brusherator Plus
* Brusherator Plus 2
* :ref:`Brusherator Preview <preview>`
* :ref:`Brusherator History <historator>`

So what's the difference between Brusherator, Brusherator Plus and Plus 2? The answer is simple: Plus and Plus 2 are copies of Brusherator, which means that all 3 panels act absolutely the same, but each remembers its own settings. This allows to have different shelves opened in the same time, one panel can be horizontal and have shelves visible as tabs, while second can be horizontal and have actions on it, with a third being square with menu items: there're a lot of possibilities to efficiently customzie your workspace. Or you can completely ignore both Pluses: it's up to you.

An example of having different shelves opened in all three Brusherators:

.. image:: img/pluses.png