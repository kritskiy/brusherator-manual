Showcase Gallery
================

Here are several images of what people do with Brusherator. Send me your cool configs! kritskiy.sergey@gmail.com 

------

By Michael Pavlovich:

.. image:: img/michael1.png

.. image:: img/michael2.png

.. image:: img/michael3.png

--------

By Dan:

.. image:: img/dan01.png

.. image:: img/dan02.png

--------

my current setup:

.. image:: img/mine.png