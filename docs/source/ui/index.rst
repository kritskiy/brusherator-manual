Interface and settings
======================

Interface
---------

.. image:: img/ui-01.png

CC2013 Differences
~~~~~~~~~~~~~~~~~~

Note that interface and hotkeys for the first Photoshop CC (2013) are slightly different because it lacks some functionality of modern Photoshops: like Fly-out menu for instance.

.. image:: img/ui-02.png

---------

Fly-out menu
~~~~~~~~~~~~

.. image:: img/flyout.png

* ``Transfer Buttons`` is available when :ref:`Edit Mode is active <editmode>`

---------

Settings
--------

Settings are available from Fly-out menu *(CC2014+)* and ``Settings`` button in the Bottom menu. Settings are unique for Brusherator, Brusherator Plus and Plus 2.

.. image:: img/ui-03settings.png

---------

Preserve Brush Size
~~~~~~~~~~~~~~~~~~~

With this option enabled, next brush selected on the panel will inherit current tool size

.. image:: img/ui-03preserve.png

---------

Preserve Brush Settings
~~~~~~~~~~~~~~~~~~~~~~~~

This is a new option in the version ``1.6.0`` and will work only in Photoshops from CC2015.5. With this option enabled, next brush selected while holding a ``Shift`` key will inherit some of the current tool settings (``Opacity``, ``Blending mode``, ``Flow``, ``Wetness``, ``Load``, ``Strength``). 

For example I have a brush with 40% opacity and click on a Round Brush preset that has 100% by default without ``Shift`` key on the left and with the key on the right. Notice how Round Brush preset inherited 40% opacity of the previous tool.

.. image:: img/preserve-settings.gif

---------

Show Names on Image Buttons
~~~~~~~~~~~~~~~~~~~~~~~~~~~

With this option selected, buttons with thumbnails will have their names shown:

.. image:: img/ui-03imagebuttons.png

---------

Hide Panel on Button Click
~~~~~~~~~~~~~~~~~~~~~~~~~~

Clicking a button will hide the panel. Reopen it with :ref:`hotkey <hotkeys>`.

---------

Highlight Current Button
~~~~~~~~~~~~~~~~~~~~~~~~

Last clicked button is highlighted with this option turned on.

---------

Show Shelves as Tabs
~~~~~~~~~~~~~~~~~~~~

Shelves are visible on the panel.

---------

Resize to Content
~~~~~~~~~~~~~~~~~

Switching between shelves will optimize the size of the panel *(CC2015+)*.

--------------------------------

Mute Panel Colors
~~~~~~~~~~~~~~~~~~

Turn this option on to mute background colors of the panel buttons. This function can be assigned to a :ref:`hotkey <hotkeys>`.

.. image:: img/mute.gif