Add Buttons to the panel
========================

You can add buttons to the panel from:

* RMB click on empty space and select ``Add...`` (button will be created in mouse cursor position)
* ``+`` button in the Bottom menu (button will be created in the top-left corner of the panel)
* ``Ctrl``/``Cmd+Click`` on empty space *(CC2014+)*  (button will be created in mouse cursor position)

.. image:: img/variants.png

-----

.. _addwindow:

Add Button window
-----------------

Add Button window consists of 3 sections: 

* Which preset the button will trigger
* How the button will appear on the panel
* Options for button appearance
* Name to appear on the panel and tooltip title to show on mouse hover (new in 1.4.2)
* ``Use preset size`` option to ignore the global ``Preserve Brush Size`` setting
* ``Create Preview`` option to create preview for Brusherator Preview panel (new in 1.5.1. Creating brush previews takes some time so if you don't use previews or wish to make your own it doesn't make a lot of sense to automatically create them all the time)

.. image:: img/add.png

Thumbnail types
~~~~~~~~~~~~~~~

* ``Text Buttons`` are simple and small text labels, for them you can set background color and name
* ``Auto: stroke`` and ``Auto: tip`` are auto-generated thumbnails which you can edit later, options are: size, background color and name
* with ``Custom image`` you can use any currently opened document as a thumbnail for button, options are: name and image selector

.. image:: img/thumbs.png

-----

Add... From Existing Presets
----------------------------

Use this command to add one or several buttons for existing Tool presets, Brush presets, Actions or Scripts. |br|
Hold Shift while selecting the command to set the last used settings in the Add Window

Select Preset(s) window:

.. image:: img/mask.png

Use Mask to filter items by name *(This works slow on Macs with CC2015+, blame Adobe)*. |br|
Select a row of items with Shift+Click or add items to selection with Ctrl/Cmd+Click

-----

.. _fromlastused:

Add... From Last Used Preset or Active Tool
-------------------------------------------

Use this command to add the last used Brush/Tool preset on the panel (without chosing it from the list) OR to create a new preset based on active tool. |br|
Hold Shift while selecting the command to automatically create a new preset and add a button with last used settings.

.. image:: img/addactive.png

-----

.. _fromhistory:

Add History or Selected
---------------------------

Use this command to add all the tool and brush presets that are marked as Favourites in :ref:`Brusherator History <historator>` to Brusherator or simply click through brushes and tools in Brush/Tools Presets window.

.. image:: img/selected.gif

-----

Add... From Active Document
---------------------------

Use this command to create a new Brush or Tool preset based on any opened document (analogue of Edit > Define Brush Preset) |br|
Hold Shift while selecting the command to automatically create a new preset from the upmost document and add a button with last used settings. |br|
Update the preset after making necessary changes from :ref:`Edit Button window <updatePreset>`

.. image:: img/fromimage.png

--------------------------------

.. _addtextlabel:

Add... a label
--------------------------------

Use this command to add a text or thumbnail label to a panel which may be useful for labeling categories of buttons.

.. image:: img/label.png

--------------------------------

.. _addmenuitem:

Add... a menu item
--------------------------------

Use this command to add most of the Photoshop menu and flyout menu items to a panel. Here's an example of adding a ``New Layer`` command, using the Search bar:

.. image:: img/menuitem.png

* Ctrl/Cmd-click on a menu item button will suppress a dialog (if applicable). For example, clicking on a New File menu item button will show a New File options window, Ctrl/Cmd-clicking will create a document with last used settings.

.. _addmenuitemfly:

--------------------------------

.. |br| raw:: html

   <br />