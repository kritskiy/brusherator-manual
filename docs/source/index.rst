Brusherator manual
==================

.. image:: img/header.gif

What is Brusherator?

In few words, it is a Photoshop panel that allows you to have a quick access to pecific

* brush and tool presets
* actions
* scripts
* and Photoshop menus

---------

In further detail, Brusherator is a system of several panels and scripts that will allow you to work with high efficiency and speed. It's not a brush manager that will spit all the tools you have at you, but rather like a highly customisable canvas where you have all the tools you need, all in the right places. |br|\
And you can have several canvases for different tasks. |br|\
And also you can switch between them in a press of the button.

.. image:: img/01.gif

--------

Do you or your company use custom scripts and actions for Photoshop? Since Brusherator allows placement of Actions, Scripts and Photoshop Menu Items, you can have them all on in front of you whenever you need them.

.. image:: img/tools.png

Contact me at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
`Discord <https://discord.gg/RTJydTg>`_ |br|\
Grab the extension on `Cubebrush <http://cbr.sh/6slma>`_ or `Gumroad <https://gum.co/brusherator>`_ |br|\
Current version is ``1.7.1``

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ui/index
   addbuttons/index
   editbuttons/index
   shelves/index
   plus/index
   historator/index
   preview/index
   advanced/index
   gallery/index
   releaselog/index

.. |br| raw:: html

   <br />