Shelves
=======

Shelves are like different canvases for your tools, you can you can have Shelves for particular situations, particular interfaces, actions and whatnot. Shelves may be rearranged in the :ref:`Edit Mode <editmode>`.

-------

Manage Shelves
--------------

Open Manage Shelves window from RMB-menu on empty space or from Bottom-menu.

* You can rename, delete, switch between existing shelves, create new an import/export shelves.
* Note that active shelf can't be renamed or deleted: switch to a different shelf before doing any of these things with it 

.. image:: img/intro.png

--------

Using Hotkeys
--------------

You can use hotkeys to switch between shelves on Brusherator and Brusherator Plus. Read more in :ref:`Advanced Features section <scripts>`

Data Folder
-----------

All shelves are .txt files in your Data Folder. If you want to manually backup/edit them (to batch rename a lot of buttons or reassign any existing button to a different preset for instance), open Data Folder from Fly-out menu.