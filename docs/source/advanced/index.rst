Advanced Features
=================

.. _alternatives:

Alternatives for tools
----------------------

On Brusherator one button can call two presets or actions and the second preset is called an Alternative. Since both presets share the same button, usually it's convenient to have as an alternative a tool that is *similar* to the main tool. Several examples:

* Two actions: one will flip image canvas horizontally and the second — vertically;
* Hard and soft round brushes;
* Horizontal and vertical brush with the same shape
* Eyedroppers with ``Current Layer`` and ``Current & Below`` settings

To create an Alternative for Brush or Tool preset, simply hit ``Create Alternative`` button in :ref:`Edit Button <editbuttons>` window. |br|\
You can also manually create them: rename or create a preset, Action or Script that has ``_a`` suffix after the name of the main tool (like ``paintbrush`` and ``paintbrush_a``, ``doSomething.jsx`` and ``doSomething_a.jsx`` for scripts)

Buttons with Alternatives have white left-top corner. To select an Alternative, Ctrl/Cmd+Click a button.

.. image:: img/alts.png

What about scripts? ``Ctrl/Cmd+Clicking`` is reserved for using as a modifier key for `Toggletator <https://toggletator-manual.readthedocs.io/en/latest/>`_ and `Scriptorator <https://scriptorator.readthedocs.io/en/latest/index.html>`_.

----

.. _scripts:

Scripts
-------

There're several scripts that come with Brusherator, they can be :ref:`assigned to hotkeys <hotkeys>` to further improve your experience and speed:

* ``toggleAtlernative``: Toggles between Main and Alternative of last selected button;
* ``bt_cycleTools``: Cycle between two last used button on Brusherator or BT Plus;
* ``toggleBrusherator``, ``toggleBrusheratorPlus``, and ``toggleBrusheratorPlus2``: toggle visibility of Brusherator and Brusherator Plus panels;
* ``BT_brushes``: script for switching between shelves (see below)

----------

Switching between shelves with hotkey
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``BT_brushes`` script can be used to switch to a particular Shelf with a hotkey. |br|\
It'll also show the panel if was hidden.

#. Close Photoshop;
#. Go to ``*Photoshop Folder*/Presets/Scripts`` and find ``BT_brushes.jsx`` file there;
#. Duplicate as many instances of the script as you need: each instance will be used to switch to a particular shelf;
#. Rename the instances of the script with the following pattern: |br|\

	* ``BT_`` (for Brusherator) OR ``BTP_`` (for Brusherator Plus) + ``shelfname`` + ``.jsx``

Examples: |br|\
``BT_Heads.jsx`` will switch to ``Heads`` shelf of Brusherator; |br|\
``BTP_texture brushes.jsx`` wills witch to ``texture brushes`` shelf of Brusherator Plus.

If the script tries to switch to non-existing Shelf, new Shelf will be created.

--------

.. _hotkeys:

Assigning scripts to hotkeys
----------------------------

To assign scripts to hotkeys for Photoshop from CC2015.5
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Go to ``FlyOut menu > Assign Shortcuts...`` menu
#. Select the function you want to assign your key
#. Type a key and select a modifier if needed. To use F-keys simply start typing ``f`` followed by a number

.. image:: img/assign16.png

.. image:: img/assign-.png

--------------------------------

To assign scripts to hotkeys for Photoshop before CC2015.5
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Go to ``Edit > Keyboard Shortcuts`` Photoshop menu
#. Make sure that ``Shrotcuts For:`` is set to ``Applications Menu``
#. Find BT shorcuts under ``File > Scripts``
#. And assign shortcuts

.. image:: img/hotkeys.png

---------

Tips
----

* Buttons with ``Eyedropper tool`` presets will automatically switch back to painting tool
* Hold shift when adding new buttons to automatically put last used settings in appearance options
* Use ``bt_cycleTools`` :ref:`script <scripts>` to quickly switch back to prevous used tool
* Adding buttons from Active Tool is a great way to create temporary tools for specific tasks: like when painting a chain or a bunch of stones:

	* create an tip for new preset
	* use ``Add... from active document`` to create a new preset with this tip, hold Shift so it'd create automatically with last used settings
	* adjust the brush
	* use ``Update The Last Used Button Preset`` in Fly-out menu to implement the changes

.. image:: img/toolfromdoc.gif

* Message me if you encounter something weird or have new ideas to kritskiy.sergey@gmail.com

.. |br| raw:: html

   <br />