.. _preview:

Brusherator Preview
==========================

Brusherator Preview is a simple satellite panel, that shows a large stroke or custom preview when you hover over the button in Brusherator. This is useful when you have small text buttons as a reminder of how the brush acts or to describe how a script/action button works.

.. image:: img/preview.gif

Default preview size is 300x120px, to change it ``Right-Click`` anywhere on the panel and use ``Set Default Preview Size`` option.

To recreate a preview in case you changed the default size or a button, use ``Recreate Preview`` from ``Right-Click`` menu on Brusherator.